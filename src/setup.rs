//Copyright (c) 2020-2022 Stefan Thesing
//
//This file is part of Zettels.
//
//Zettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Zettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Zettels. If not, see http://www.gnu.org/licenses/.
//
//! Module to interactivly generate a user config.
//!
use std::path::{PathBuf, Path};
use std::io;
use libzettels::{Config, IndexingMethod, SequenceStart};

use config;

//-----------------------------------------------------------------------
// One public function - calling either minimal or full config
//-----------------------------------------------------------------------
/// Interactively generates a config file for zettels. Takes the designated
/// config directory and config file as parameters.
/// Asks the user for settings information and writes it to the config file.
pub fn generate_settings<P: AsRef<Path>>(config_dir: P, config_file: P) {
    let config_dir = config_dir.as_ref();
    let config_file = config_file.as_ref();

    println!("This will interactively generate a new config file.");
    println!("You can either:");
    println!("1. Do a minimal setup – 2 simple questions");
    println!("2. Do a full setup – 2 simple and 3 more complex questions.");
    println!("3. Abort");
    println!("Enter 1-3, Default is 1");
    let mut answer = String::new();
    io::stdin().read_line(&mut answer).expect("Failed to read line");
    answer = answer.trim().to_string();
    
    let mut full = false; // Default
    if answer.eq("2") {
        full = true;
    } else if answer.eq("3") {
        println!("Aborted by user.");
        std::process::exit(0);
    }
    
    let cfg = 
        if full {
            full_config(config_dir)
        } else {
            minimal_config(config_dir)
        };    
    
    // Wrap up
    println!("These are your settings:");
    config::show_settings(&cfg);
    println!("");
    println!("Are these correct? y/n, Default is 'y'.");
    let mut answer = String::new();
    io::stdin().read_line(&mut answer).expect("Failed to read input.");
    if answer.trim().eq_ignore_ascii_case("n") {
        println!("Do you want to start over? y/n, Default is 'y'.");
        let mut answer = String::new();
        io::stdin().read_line(&mut answer).expect("Failed to read input.");
        if answer.trim().eq_ignore_ascii_case("n") {
            println!("Aborted by user.");
            std::process::exit(0);
        } else {
            println!("");
            println!("Starting over");
            println!("");
            generate_settings(config_dir, config_file);
        }
    } else {
        
        println!("Writing configuration to: {}", config_file.to_str()
                                                                .unwrap());
        cfg.to_file(config_file).expect("Failed to write config to file.");
    }
}

//-----------------------------------------------------------------------
// Two main private functions
//-----------------------------------------------------------------------

fn minimal_config<P: AsRef<Path>>(config_dir: P) -> Config {
    let rootdir = ask_for_rootdir();
    let indexfile = ask_for_indexfile(config_dir);
    
    Config::new(rootdir, indexfile)
}

fn full_config<P: AsRef<Path>>(config_dir: P) -> Config {
    let mut cfg = minimal_config(config_dir);
    cfg.indexingmethod = ask_for_indexingmethod();
    cfg.sequencestart = ask_for_sequencestart();
    cfg.ignorefile = ask_for_ignorefile();
    
    cfg
}

//-----------------------------------------------------------------------
// The details
//-----------------------------------------------------------------------

fn ask_for_rootdir() -> PathBuf {
    // Default
    let mut rootdir = std::env::current_dir()
                        .expect("Failed to determine current directory.");
    // Root directory
    println!("Please specify the root directory of your Zettelkasten.");
    println!("It will contain the Zettel files.");
    println!("Default is the current directory '{}'", 
            rootdir.to_str().unwrap());
    let mut answer = String::new();
    io::stdin().read_line(&mut answer).expect("Failed to read input.");
    answer = answer.trim().to_string();
    if !answer.is_empty() {
        rootdir = PathBuf::from(answer);
    }
    debug!("Set up rootdir: {:?}", rootdir);
    rootdir
}

fn ask_for_indexfile<P: AsRef<Path>>(config_dir: P) -> PathBuf {
    let config_dir = config_dir.as_ref();
    // Default
    let mut indexfile = config_dir.join("index.yaml");
    
    // Index file
    println!("Please specify the file containing the index of your \
    Zettelkasten.");
    println!("Default is: {}", indexfile.to_str().unwrap());
    let mut answer = String::new();
    io::stdin().read_line(&mut answer).expect("Failed to read input.");
    answer = answer.trim().to_string();
    
    if !answer.is_empty() {
        indexfile = PathBuf::from(answer);
    }
    debug!("Set up indexfile: {:?}", indexfile);
    indexfile    
}

fn ask_for_indexingmethod() -> IndexingMethod {
    println!("There are three methods available for indexing:");
    println!("1. Native Method - works out of the box.");
    println!("2. Grep - probably faster, requires grep to be installed.");
    println!("3. RipGrep - probably fastest, requires ripgrep to be \
              installed.");
    println!("Choose 1-3, Default 1 (Native):");
    let mut answer = String::new();
    io::stdin().read_line(&mut answer).expect("Failed to read input.");
    answer = answer.trim().to_string();
    
    if answer.eq("2") {
        IndexingMethod::Grep
    } else if answer.eq("3") {
        IndexingMethod::RipGrep
    } else {
        IndexingMethod::Native
    }
}

fn ask_for_sequencestart() -> SequenceStart {
    println!("There are five ways for Zettels to determine which zettel \
            files to consider as the start of a zettel sequence. See \
            README for details.");
    println!("1. Root Only");
    println!("2. Keyword - Default");
    println!("3. Fork");
    println!("4. Keyword and Fork");
    println!("5. Keyword or Fork");
    println!("Choose 1-5, Default 2 (Keyword):");
    let mut answer = String::new();
    io::stdin().read_line(&mut answer).expect("Failed to read input.");
    answer = answer.trim().to_string();
    
    if answer.eq("1") {
        SequenceStart::RootOnly
    } else if answer.eq("3") {
        SequenceStart::Fork
    } else if answer.eq("4") {
        SequenceStart::KeywordAndFork
    } else if answer.eq("5") {
        SequenceStart::KeywordOrFork
    } else {
        SequenceStart::Keyword
    }
}

fn ask_for_ignorefile() -> PathBuf {
    println!("Zettels uses a file to specify patterns to be ignored by the \n\
    Zettelkasten with the same syntax as gitignore files.");
    println!("By default, it *is* `.gitignore` (because you might \n\
    manage your Zettelkasten with git).");
    println!("But in case you want different ignore-patterns for git and \n\
    your Zettelkasten, you can specify an alternative file, here (e.g. \n\
    `.zettelsignore`).");
    println!("Please enter a filename for your ignore file.");
    println!("Default is `.gitignore`.");
    let mut answer = String::new();
    io::stdin().read_line(&mut answer).expect("Failed to read input.");
    answer = answer.trim().to_string();
    
    if !answer.is_empty() {
        PathBuf::from(answer)
    } else {
        PathBuf::from(".gitignore")
    }
}


